# AWS-Challenge 
The goal of this challenge was to make the trio-task repository and deploy it on AWS, using cloud infrastructure and architecture in mind.

![Infastructure-diagram](images/Diagram.png)

## contents
* [VPC](#VPC)
* [Security group](#Security-group)
* [Route Table](#Route-table)
* [Subnet group](#Subnet-group)
* [RDS](#RDS)
* [EC2](#EC2)
* [Reachabilty Analyzer](#Reachabilty-Analyzer)

# VPC
My first action was to create a VPC with both public and private subnets. I made sure I first had an elastic IP address that would allow me to have a NAT gateway. 

 ![Elastic IP](images/Screenshot_98.png)

 Here i named my VPC connected it to my elastic IP address and changed the public subnets IPv4 CIDR

![Elastic IP](images/Screenshot_99.png)

My public and private subnets were made and given allocating names in accordance. As the architecture I was trying to create had two private subnets I then made another one to duplicate that.We needed to make more than one subnet bc my RDS will need a subnet group for it to work later in the process 

![Elastic IP](images/Screenshot_107.png)

# Security group

I made a security group for my VPC, this acts as a virtual firewall for my VPC I set what inbound and outbound rules control incoming and outgoing traffic to my VPC, I made separate security groups for my RDS and EC2 instance as well.
![Security-group](images/Screenshot_108.png)

# Route table 

I made 2 route tables one public leading to the internet gateway   
![Route table](images/Screenshot_109.png)

one leading to the NAT gateway

![Route table](images/Screenshot_110.png)

# Subnet group

A subnet group was created for the VPC that enables me to designate for my DB instance. I can specify a particular VPC when creating my RDS.
![Route table](images/Screenshot_111.png)

# RDS 
whilst configuring my DB I had options that let me connect to my already made subnets, this is where I think the order I created my architecture made it easier for me. 
![Route table](images/Screenshot_103.png) 

# EC2
Creating the EC2 was fairly simple and I made all the connections needed and launched up the terminal on my visual studios. I edited the pymysql connecting string to match one of my RDS instances. Then I covered them with specific environment variables for added security so no one can see my secrets. 
![Route table](images/Screenshot_106.png)

For added security i changed an inbound rule to only allow my IP address to SSH into my EC2 instance 
![Route table](images/Screenshot_112.png)

Docker and NGINIX was installed on my EC2
![Route table](images/Screenshot_113.png)

When I Ran the app this is the desired outcome

![Route table](images/Screenshot_114.png)

# Reachabilty Analyzer 
Whilst playing around with aws I stumbled upon a reachability analyzer, I wasn't sure what its function was, however after some time testing with it, it turned out to be a useful tool it let me see how my EC2 gets to the internet gateway and all the steps it takes. I feel this is a resourceful tool that I will be using for my future projects. 
![Route table](images/Screenshot_105.png) 